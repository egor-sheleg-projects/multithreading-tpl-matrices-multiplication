# multithreading-tpl-matrices-multiplication
------------------------------------------------------------------
**The purpose of the task** is to explore the opportunities offered by the TPL library for parallel programming. 

**Task Description** 

1. Clone the [repository](https://gitlab.com/epam-autocode-tasks/multithreading-tpl-matrices-multiplication). 

1. Read the code provided in the template. You will see two classes for multiplying matrices: 

    - MatricesMultiplier 

    - MatricesMultiplierParallel 

1. Fill in the missing code for the IMatrix Multiply method (IMatrix m1, IMatrix m2) in the MatricesMultiplierParallel class so that multiplication is executed with Parallel.ForEach.
1. Make sure that MultiplierTest: MultiplyMatrix3On3TestForParallelMultiplier executes successfully. 

1. Implement the test method in the class MultiplierTest. The method should:
    - create two matrices of size N with random values 
    - estimate time to multiply the matrices with the MatricesMultiplier and MatricesMultiplierParallel classes. 

Experiment with the size of the matrices to determine the size at which parallel multiplicaiton takes less time than regular multiplication. 


The following materials may be helpful: 

 - [Task Parallel Library (TPL)](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-parallel-library-tpl) 

 - [How to: Write a Simple Parallel.ForEach Loop](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/how-to-write-a-simple-parallel-foreach-loop) 

 - [Stopwatch Class](https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.stopwatch?view=net-5.0) 

