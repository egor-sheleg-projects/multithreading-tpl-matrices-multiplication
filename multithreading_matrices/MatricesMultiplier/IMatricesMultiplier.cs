﻿namespace multithreading_matrixes.MatrixMultiplier
{
    public interface IMatricesMultiplier
    {
        IMatrix Multiply(IMatrix m1, IMatrix m2);
    }
}
